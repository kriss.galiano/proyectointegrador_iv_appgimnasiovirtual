package pe.edu.tecsup.ViGym

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.util.jar.Attributes

class MainActivity_Inicio : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_inicio)

        val lista = findViewById<FloatingActionButton>(R.id.fab)
        lista.setOnClickListener {
            val listado = Intent(this, ListaCursosActivity::class.java)
            startActivity(listado)

        }
    }
}
