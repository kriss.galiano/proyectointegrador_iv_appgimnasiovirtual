package pe.edu.tecsup.ViGym

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.schedulers.Schedulers
import io.reactivex.android.schedulers.AndroidSchedulers
import pe.edu.tecsup.ViGym.adapters.ListadoAdapter


class ListaCursosActivity : AppCompatActivity() {

    private lateinit var rvListaFrases : RecyclerView

    private var adapterListaFrases = ListadoAdapter()

    var listFrases = ArrayList<Cursos>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_cursos)

        setView()
        setAdapter()
        getListaCursos()
    }

    private fun setView() {
        rvListaFrases = findViewById(R.id.rvListaFrases)
    }

    private fun setAdapter() {
        adapterListaFrases.list = listFrases
        rvListaFrases.adapter = adapterListaFrases
    }

    private fun getListaCursos() {

        var retrofit =  RetrofitHelper.getRetrofit()

        var listdeCurso  = retrofit.create(ApiClient::class.java).getList().subscribeOn(
            Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                adapterListaFrases.list = response
            }, { error ->
                Toast.makeText(this,error.message.toString(),Toast.LENGTH_LONG).show()
            })
    }
}