package pe.edu.tecsup.ViGym


class Cursos (
    val id: String,
    val nombre: String,
    val descripcion: String,
    val sesiones: Int,
    val capacidad: Int,
    val precio: Int,
    val calorias_perdidas: Int,

    var author : String,
    var quote : String
)